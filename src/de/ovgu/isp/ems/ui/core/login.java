package de.ovgu.isp.ems.ui.core;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import de.ovgu.isp.ems.core.Localization;
import de.ovgu.isp.ems.core.Theme;
import de.ovgu.isp.ems.db.db;
import de.ovgu.isp.ems.db.Emp;
import javax.swing.DefaultComboBoxModel;
import de.ovgu.isp.ems.core.AccessControl;
import java.sql.SQLException;

public class login {
	
	private String sql;
	
	public void setSQLStatement(String s){
		System.out.println("Setting SQL Statement to: "+ s);
		this.sql = s;
		System.out.println(sql);
	}
	
	private String getLoginQuery(){
		return this.sql;
	}

	Connection conn = null;

	ResultSet rs = null;

	PreparedStatement pst = null;
	Scanner scan = new Scanner(System.in);

	String role = "Admin"; // TODO: replace user role

	/**
	 * This method initializes UI fields consisting of user name, password und
	 * user role fields
	 */
	private String initPwdfield() {
		System.out.println(messages.getString("pwd") + ":");
		String password = scan.next();
		return password;
		
		
	}
	
	private String initUserfield(){
		System.out.println(("user") + ":");
		String username = scan.nextLine();
		return username;
		
	}

	/**
	 * @return selected user role for login
	 */
	public String getSelectedUserRole() {
		// TODO: get role
		String s = "";
		return s;
	}

	// GEN-END:hook methods

	/**
	 * login construtors
	 */
	public login() {
		System.out.println("Starting EMS Application...");
		conn = db.java_db(); // starting DB connection
		initComponents();
		currentDate();

	}

	public login(String s) {

	}

	// GEN-BEGIN:initComponents
	private void initComponents() {

		ArrayList<Option> optionList = new ArrayList<Option>();
		/**
		 * login window configuration:
		 */

		optionList.add(new Option("Exit", new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		}));

		System.out.println(messages.getString("login"));
		System.out.println(messages.getString("enterlogindata"));
		String username = initUserfield();
		String password = initPwdfield();
		

		loginPerformed(username, password);

		// TODO: pointcut implementation of user role in 3FA
		System.out.println();

		// TODO: pointcut implementation of setting color and font size on
		// console
		// set background:
		Theme t = new Theme();
		t.getColor();
		t.getSize();

	}

	// GEN-END:initComponents

	/**
	 * This method sets current Time Stamp as text to Menu
	 */
	public void currentDate() {

		/**
		 * set Date Menu Text with local date format
		 */
		String dateString = new Localization().getCountrySpecificDate();
		System.out.println(dateString);
	}

	private void loginPerformed(String user, String pwd) {// GEN-FIRST:event_cmd_LoginActionPerformed

		if (user.equals("")) {
			JOptionPane.showMessageDialog(null,
					messages.getString("empty_username_field"));
		}
		/**
		 * check if password field is empty
		 */
		else if (pwd.equals("")) {
			JOptionPane.showMessageDialog(null,
					messages.getString("empty_password_field"));
		} else {
			System.out.println("Proceeding....\nPreparing SQL statement");
			/**
			 * Prepare SQL statement to check if credentials are valid
			 */
			/**
			 * if 3FA --> additional check if selected role matches user's role
			 */
			try {
				int count = 0; // Login-Status

				/**
				 * Prepare SQL statement: SELECT * FROM USERS WHERE input values
				 * are maAdmintching
				 */
				System.out.println("SQL: "+getLoginQuery());
				pst = conn.prepareStatement(getLoginQuery());
				System.out.println("Trying");

				pst.setString(1, user);
				pst.setString(2, pwd);
				pst.setString(3, role);

				/**
				 * Execute Query
				 */
				rs = pst.executeQuery();
				System.out.println("Executing SQL login query.");
				while (rs.next()) {
					System.out.println("Result set is not empty.");

					int id = rs.getInt(1);
					Emp.empId = id;
					count = count + 1; // count=1 : login allowed
				}

				/**
				 * if user role is valid:
				 */
				if (role.toUpperCase().equals("ADMIN")
						|| role.toUpperCase().equals("SALES")) {
					if (count == 1) {
						System.out.println("Successfully logged in..."
								+ messages.getString("sucess"));

						JOptionPane.showMessageDialog(null,
								messages.getString("sucess"));

						/**
						 * close login window:
						 */
						/**
						 * Create new Date object to store session time stamp in
						 * TABLE AUDITS
						 */
						Localization _loc = new Localization();
						System.out.println("Successfully logged in...");

						/**
						 * insert session into database
						 */
						int value = Emp.empId;
						System.out.println("Successfully logged in...");

						String reg = "insert into Audit (emp_id,date,status) values ('"
								+ value
								+ "','"
								+ _loc.getTimeString()
								+ " / "
								+ _loc.getDateString() + "','Logged in')";
						pst = conn.prepareStatement(reg);
						pst.execute();
						/**
						 * close login window
						 */
						System.out.println("Successfully logged in to EMS.");
						// TODO: clear Console
						Runtime.getRuntime().exec("CLEAR");
						// TODO: Open MainMenu Dialog
						MainMenu m = new MainMenu();

					} else if (count > 1) {
						JOptionPane.showMessageDialog(null,
								messages.getString("duplicate_login"));
					} else {
						JOptionPane.showMessageDialog(null,
								messages.getString("incorrect_password"));
					}
				} else {
					System.err.println("User role not defined!");
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e);
				e.printStackTrace();

			} finally {
				try {
					rs.close();
					pst.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	// GEN-LAST:event_cmd_LoginActionPerformed

	/**
	 * @param args
	 *            no command line arguments
	 */
	public static void main(String args[]) {
		new login();

	}

	AccessControl ac = new AccessControl();

	// default values:
	public static Locale currentLocale = new Locale("en", "US");

	public ResourceBundle messages = new Localization().getBundle();

	private JPanel jPanel2 = new JPanel();

	JLabel jLabel3 = new JLabel();

	private JComboBox txt_combo = new JComboBox();

}
