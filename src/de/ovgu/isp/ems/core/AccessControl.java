package de.ovgu.isp.ems.core; 


//Three factors (user name + password + user role) are needed to perform a login
public   class  AccessControl {
	

	public AccessControl() {

	}

	
	
	/**
	 * ADD NEW USER ROLES HERE:
	 * 
	 * 
	 * @return a list of valid user roles
	 */
	public String[] getAllUserRoles() {
		String[] userroles = new String[3];
		userroles[0] = "Admin";
		userroles[1] = "Sales";
		userroles[2] = "HR";
		return userroles;

	}

	

	
	// method returns Login Query for user name + password + role combination
	public String getLoginQuery() {
		return "select id,username,password,division from Users Where (username =? and password =? and division =?)";
	}


}
