package de.ovgu.isp.ems.core;

import java.awt.Color;
import java.awt.Font;

public aspect Theme3 {

	Color around() : execution(Color Theme.getColor()){
		Color _color = Color.RED;
		return _color;
	}
	
	Font around() : execution(Font Theme.getFont()) {
		Font _font = new Font("Verdana", Font.BOLD, 12);
		return _font;
	}
	
	String around() : execution(String Theme.getName()){
		String _name = "Verdana";
		return _name;
	}
	
	int around() : execution(int Theme.getSize()){
		int _size = 11;
		return _size;
	}

	String around() : execution(String Theme.getThemeBackground()){
		String _imageLocation = "/Images/bk3.jpg";
		return _imageLocation;

	}
}
