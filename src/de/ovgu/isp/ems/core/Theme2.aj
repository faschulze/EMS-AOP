package de.ovgu.isp.ems.core;

import java.awt.Color;
import java.awt.Font;

public aspect Theme2 {
	
	Color around() : execution(Color Theme.getColor()){
		Color _color = Color.BLUE;
		return _color;
	}
	
	Font around() : execution(Font Theme.getFont()) {
		Font _font = new Font("Arial", Font.BOLD, 12);
		return _font;
	}
	
	String around() : execution(String Theme.getName()){
		String _name = "Arial";
		return _name;
	}
	
	int around() : execution(int Theme.getSize()){
		int _size = 12;
		return _size;
	}

	String around() : execution(String Theme.getThemeBackground()){
		String _imageLocation = "/Images/bk.jpg";
		return _imageLocation;

	}

}
