package de.ovgu.isp.ems.logging; 

import java.io.File; 

import org.apache.logging.log4j.LogManager; 
import org.apache.logging.log4j.Logger; 

public  class  ApplicationLogger {
	
	private Logger logger = LogManager.getRootLogger();

	

	public ApplicationLogger() {
		makeDirectory();
	}

	

	public void setLevel(String s) {
	}

	

	public Logger getApplicationLogger() {
		return logger;
	}

	

	private void makeDirectory() {
		File logDirectory = new File("." + File.separator + "logs");
		// if the directory does not exist, create it
		if (!logDirectory.exists()) {
			System.out.println("creating directory: " + logDirectory.getName());
			boolean result = false;

			try {
				logDirectory.mkdir();
				result = true;
			} catch (SecurityException se) {
				// handle it
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
	}


}
